﻿using Microsoft.AspNetCore.Mvc;
using UltimateQuarry.Data.Abstract;
using System.Threading.Tasks;
using UltimateQuarry.Manager;
using UltimateQuarry.Manager.ViewModels;

namespace UltimateQuarry.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        private IUserManager _userManager;
        public UserController(IUserFeedbackRepository userFeedbackRepository, IUserManager userManager)
        {
            _userManager = userManager;
        }
        /// <summary>
        /// Get All User Feedbacks
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Get()
        {
            var userFeedbacks =_userManager.GetAllUserFeedbacks();
            return new OkObjectResult(userFeedbacks);
        }
        /// <summary>
        /// Get Feedback by FeedbackId
        /// </summary>
        /// <param name="feedbackId"></param>
        /// <returns></returns>
        [HttpGet("{feedbackId}", Name = "GetUserFeedbackInfo")]
        public async Task<IActionResult> GetFolderSecuritySettingById(int feedbackId)
        {
            var response = await Task.Run(() => _userManager.GetFeedbackInfo(feedbackId));
            return new OkObjectResult(response);
        }

        /// <summary>
        /// Saving Feedback
        /// Test url and Object- POST
        /// http://localhost:8000/api/user
        ///  {
        ///    "feedback": "new feedback test",
        ///    "starRating": 3,
        ///    "email": "abc@1234.com",
        ///    "userName": "abc"
        ///  }
        /// </summary>
        /// <param name="userFeedback"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> Create([FromBody] UserFeedbackViewModel userFeedback)
        {
            var response = await Task.Run(() => _userManager.CreateUserFeedback(userFeedback));
            if (response.ErrorInfo.HasErrors)
                return new OkObjectResult(response.ErrorInfo.ErrorList);
            else
            {
                CreatedAtRouteResult result = CreatedAtRoute("GetUserFeedbackInfo", new { controller = "User", feedbackId = response.ReferenceId }, response.ObjectValue);
                return result;

            }
        }
      
    }

}
