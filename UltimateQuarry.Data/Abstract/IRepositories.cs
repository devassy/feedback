﻿using UltimateQuarry.Model;

namespace UltimateQuarry.Data.Abstract
{
   /// <summary>
   /// In a more complex system all the interfaces would have its own class
   /// </summary>
    public interface IUserFeedbackRepository : IEntityBaseRepository<UserFeedback> { }


}
