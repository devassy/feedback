﻿using Microsoft.EntityFrameworkCore;
using UltimateQuarry.Model;


namespace UltimateQuarry.Data
{
    public class UltimateQuarryContext : DbContext
    {
       public DbSet<UserFeedback> UserFeedbacks { get; set; }
               
       public UltimateQuarryContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
           
           
        }
    }
}
