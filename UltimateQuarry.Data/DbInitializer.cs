﻿using UltimateQuarry.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UltimateQuarry.Data
{
    public class DbInitializer
    {
        private static UltimateQuarryContext context;
        public static void Initialize(IServiceProvider serviceProvider)
        {
            context = (UltimateQuarryContext)serviceProvider.GetService(typeof(UltimateQuarryContext));
            
            InitializeUltimateQuarryData();
        }
        public static void Initialize(UltimateQuarryContext ultimateQuarryContext)
        {
            context = ultimateQuarryContext;
            InitializeUltimateQuarryData();
        }
        /// <summary>
        /// Initializing a sample data set for the testing.
        /// </summary>
        private static void InitializeUltimateQuarryData()
        {
            
            if (context != null)
            {
              

                UserFeedback userFeedback = new UserFeedback() { UserName = "abc", DateAndTime = DateTime.UtcNow, Email = "abc@123.com", Feedback = "new feedback", StarRating = 4 , DateTimeOffsetValue = DateTime.Now};
                context.UserFeedbacks.Add(userFeedback);

            }

                context.SaveChanges();
        }
    }
}
