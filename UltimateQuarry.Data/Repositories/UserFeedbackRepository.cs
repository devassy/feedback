﻿using UltimateQuarry.Model;
using UltimateQuarry.Data.Abstract;

namespace UltimateQuarry.Data.Repositories
{
    public class UserFeedbackRepository : EntityBaseRepository<UserFeedback>, IUserFeedbackRepository
    {
        public UserFeedbackRepository(UltimateQuarryContext context)
            : base(context)
        { }
    }
}
