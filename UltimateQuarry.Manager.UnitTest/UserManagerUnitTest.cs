using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using UltimateQuarry.Data;
using UltimateQuarry.Data.Abstract;
using UltimateQuarry.Data.Repositories;
using UltimateQuarry.Manager.Validations;
using UltimateQuarry.Manager.ViewModels;

namespace UltimateQuarry.Manager.UnitTest
{
    [TestClass]
    public class UserManagerUnitTest
    {
        private IUserManager _userManager { set; get; }
        public UserManagerUnitTest()
        {
            _userManager = InitializeUserManager();

        }
        private IUserManager InitializeUserManager()
        {
            IUserFeedbackRepository iUserFeedbackRepository =  InitializeFeedbackRespository();
            IValidationManager iValidationManager = InitializeValidationManager();
            IUserManager iUserManager = new UserManager(iUserFeedbackRepository, iValidationManager);
            return iUserManager;
        }
        private IValidationManager InitializeValidationManager()
        {
            IUserFeedbackRepository iUserFeedbackRepository = InitializeFeedbackRespository();
            IValidator<UserFeedbackViewModel> feedbackValidator = new UserFeedbackViewModelValidator();
            IValidationManager validationManager = new ValidationManager(feedbackValidator, iUserFeedbackRepository);
            return validationManager;
        }

        private static IUserFeedbackRepository InitializeFeedbackRespository()
        {
            
            var options = new DbContextOptionsBuilder<UltimateQuarryContext>()
                 .UseInMemoryDatabase(databaseName: "MyDb")
                 .Options;
            var dbContext = new UltimateQuarryContext(options);
            IUserFeedbackRepository iUserFeedbackRepository = new UserFeedbackRepository(dbContext);
            return iUserFeedbackRepository;
        }

        [TestMethod]
        public void SaveUserFeedBack()
        {
            UserFeedbackViewModel userFeedbackViewModel = new UserFeedbackViewModel() { UserName = "abc", DateAndTime = DateTime.UtcNow, Email = "abc@123.com", Feedback = "new feedback", StarRating = 4, DateTimeOffsetValue = DateTime.Now };
            var result = _userManager.CreateUserFeedback(userFeedbackViewModel);
            Assert.IsTrue(result.ErrorInfo.HasErrors == false);
        }

        [TestMethod]
        public void ValidateDuplicateEntry()
        {
            UserFeedbackViewModel userFeedbackViewModel = new UserFeedbackViewModel() { UserName = "abc", DateAndTime = DateTime.UtcNow, Email = "abc@123.com", Feedback = "new feedback", StarRating = 4, DateTimeOffsetValue = DateTime.Now };
            var result = _userManager.CreateUserFeedback(userFeedbackViewModel);
            userFeedbackViewModel = new UserFeedbackViewModel() { UserName = "abc", DateAndTime = DateTime.UtcNow, Email = "abc@123.com", Feedback = "new feedback", StarRating = 4, DateTimeOffsetValue = DateTime.Now };
            result = _userManager.CreateUserFeedback(userFeedbackViewModel);
            Assert.IsTrue(result.ErrorInfo.HasErrors == true);
            Assert.IsTrue(result.ErrorInfo.ErrorList.Contains("Cannot submit more than one feedback per user"));
        }

        [TestMethod]
        public void ValidateUserFeedBack()
        {
            UserFeedbackViewModel userFeedbackViewModel = new UserFeedbackViewModel() { UserName = "1abc", DateAndTime = DateTime.UtcNow, Email = "abc.com", Feedback = "new feedback", StarRating = 8, DateTimeOffsetValue = DateTime.Now };
            var result = _userManager.CreateUserFeedback(userFeedbackViewModel);
            Assert.IsTrue(result.ErrorInfo.HasErrors == true);
            Assert.IsTrue(result.ErrorInfo.ErrorList.Contains("'Email' is not a valid email address."));
            Assert.IsTrue(result.ErrorInfo.ErrorList.Contains("'User Name' is not in the correct format."));
            Assert.IsTrue(result.ErrorInfo.ErrorList.Contains("'Star Rating' must be between 1 and 5. You entered 8."));
        }
    }
}
