﻿using System;
using System.Collections.Generic;
using System.Text;
using UltimateQuarry.Manager.ViewModels;

namespace UltimateQuarry.Manager
{
   public interface IUserManager
    {
        UserFeedbackViewModel GetFeedbackInfo(int feedbackId);
        IEnumerable<UserFeedbackViewModel> GetAllUserFeedbacks();
        ResponseResult CreateUserFeedback(UserFeedbackViewModel userFeedbackVm);
    }
}
