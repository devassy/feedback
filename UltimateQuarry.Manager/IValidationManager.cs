﻿using System;
using System.Collections.Generic;
using System.Text;
using UltimateQuarry.Manager.ViewModels;

namespace UltimateQuarry.Manager
{
   public interface IValidationManager
    {
        ErrorInfo ValidateUserFeedback(UserFeedbackViewModel userFeedbackVm); 
    }
}
