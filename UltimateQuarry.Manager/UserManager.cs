﻿using AutoMapper;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using UltimateQuarry.Data.Abstract;
using UltimateQuarry.Manager.ViewModels;
using UltimateQuarry.Model;

namespace UltimateQuarry.Manager
{
    public class UserManager : IUserManager
    {
        private IUserFeedbackRepository _userFeedbackRepository;
        private IValidationManager _validationManager;

        public UserManager(IUserFeedbackRepository userFeedbackRepository, IValidationManager validationManager)
        {
            _userFeedbackRepository = userFeedbackRepository;
            _validationManager = validationManager;
        }
        /// <summary>
        /// Get Feedback object based on FeedbackId
        /// </summary>
        /// <param name="feedbackId"></param>
        /// <returns></returns>
        public UserFeedbackViewModel GetFeedbackInfo(int feedbackId)
        {
            var _feedbackInfo = GetFeedback(feedbackId);
            UserFeedbackViewModel _feedbackInfoVM = Mapper.Map<UserFeedback, UserFeedbackViewModel>(_feedbackInfo);
            return _feedbackInfoVM;
        }
        private UserFeedback GetFeedback(int feedbackId)
        {
            var _feedbackInfo = _userFeedbackRepository
                .GetSingle(f => f.Id == feedbackId);
            return _feedbackInfo;
        }
        /// <summary>
        /// Get All UserFeedbacks
        /// </summary>
        /// <returns></returns>
        public IEnumerable<UserFeedbackViewModel> GetAllUserFeedbacks()
        {
            IEnumerable<UserFeedback> _feedbacks = _userFeedbackRepository
                .GetAll()
                .ToList();

            IEnumerable<UserFeedbackViewModel> _userFeedbackVM = Mapper.Map<IEnumerable<UserFeedback>, IEnumerable<UserFeedbackViewModel>>(_feedbacks);
            return _userFeedbackVM;
        }
        /// <summary>
        /// Create UserFeedabck after valiadting of Business Rules
        /// </summary>
        /// <param name="userFeedbackVm"></param>
        /// <returns></returns>
        public ResponseResult CreateUserFeedback(UserFeedbackViewModel userFeedbackVm)
        {
            var response = InsertUserFeedback(userFeedbackVm);
             return response;
        }
        private ResponseResult InsertUserFeedback(UserFeedbackViewModel userFeedbackVm)
        {
            ResponseResult response = new ResponseResult();
            //Validate Business Rules
            var results = _validationManager.ValidateUserFeedback(userFeedbackVm);
            if(!results.HasErrors)
            {
                //Insert UserFeedback
                 UserFeedback _feedback = new UserFeedback { Feedback = userFeedbackVm.Feedback, UserName = userFeedbackVm.UserName, Email = userFeedbackVm.Email, DateAndTime = DateTime.UtcNow, DateTimeOffsetValue = DateTime.Now, StarRating = userFeedbackVm.StarRating };
                _userFeedbackRepository.Add(_feedback);
                _userFeedbackRepository.Commit();
                response.ReferenceId = _feedback.Id;
                //userFeedbackVm = Mapper.Map<UserFeedback, UserFeedbackViewModel>(_feedback);
                response.ObjectValue = _feedback;
                response.Message = "Saved Successfully";
            }
            else
            {
                //Set ErrorInfo with the results
                response.ErrorInfo = results;
            }
            
            return response;
        }

    }
}
