﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UltimateQuarry.Data.Abstract;
using UltimateQuarry.Manager.ViewModels;

namespace UltimateQuarry.Manager
{
    public class ValidationManager: IValidationManager
    {
        private IValidator<UserFeedbackViewModel> _feedbackValidator;
        private IUserFeedbackRepository _userFeedbackRepository;
        public ValidationManager(IValidator<UserFeedbackViewModel> feedbackValidator, IUserFeedbackRepository userFeedbackRepository)
        {
            _userFeedbackRepository = userFeedbackRepository;
            _feedbackValidator = feedbackValidator;
        }
        /// <summary>
        /// For Validating business rules for UserFeedback
        /// </summary>
        /// <param name="userFeedbackVm"></param>
        /// <returns></returns>
        public ErrorInfo ValidateUserFeedback(UserFeedbackViewModel userFeedbackVm)
        {
            ErrorInfo errorInfo = new ErrorInfo();

            if (userFeedbackVm != null)
            {
                //perform validation on the input - Model Validation
                var modelValidationResults = _feedbackValidator.Validate(userFeedbackVm);
                if (modelValidationResults.IsValid)
                {
                    //Make database call to find if the user already has a feedback submitted.
                    //Assumption: Unique Email address is used for finding the duplicate feedback for a user.
                    var _feedbackCount = _userFeedbackRepository
                   .FindBy(f => f.Email == userFeedbackVm.Email).Count();
                    if (_feedbackCount > 0)
                    {
                        errorInfo.HasErrors = true;
                        errorInfo.ErrorList.Add("Cannot submit more than one feedback per user");
                    }

                }
                else
                {
                    errorInfo.HasErrors = true;
                    errorInfo.ErrorList.AddRange(modelValidationResults.Errors.Select(x => x.ErrorMessage).ToList());
                }
            }
            else
            {
                errorInfo.HasErrors = true;
                errorInfo.ErrorList.Add("Invalid UserFeedback Object - Please check the input object");
            }

            return errorInfo;
            
        }
    }
}
