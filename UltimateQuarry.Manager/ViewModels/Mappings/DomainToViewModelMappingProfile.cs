﻿using AutoMapper;
using UltimateQuarry.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using UltimateQuarry.Manager.ViewModels;

namespace UltimateQuarry.Manager.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
             CreateMap<UserFeedback, UserFeedbackViewModel>();


        }
    }
}
