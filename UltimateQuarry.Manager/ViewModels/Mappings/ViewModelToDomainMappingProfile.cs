﻿using AutoMapper;
using UltimateQuarry.Model;
using UltimateQuarry.Manager.ViewModels;

namespace UltimateQuarry.Manager.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
           CreateMap<UserFeedbackViewModel, UserFeedback>();

        }
    }
}
