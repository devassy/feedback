﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using UltimateQuarry.Manager.Validations;

namespace UltimateQuarry.Manager.ViewModels
{
    public class UserFeedbackViewModel : IValidatableObject
    {
        public int Id { get; set; }
        public string Feedback { get; set; }
        public float StarRating { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime DateAndTime { get; set; }
        public DateTimeOffset DateTimeOffsetValue { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var validator = new UserFeedbackViewModelValidator();
            var result = validator.Validate(this);
            return result.Errors.Select(item => new ValidationResult(item.ErrorMessage, new[] { item.PropertyName }));
        }
    }
}
