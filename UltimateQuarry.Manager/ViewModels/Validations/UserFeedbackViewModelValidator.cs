﻿using FluentValidation;
using System;
using UltimateQuarry.Manager.ViewModels;

namespace UltimateQuarry.Manager.Validations
{
    public class UserFeedbackViewModelValidator : AbstractValidator<UserFeedbackViewModel>
    {
        public UserFeedbackViewModelValidator()
        {

            //Rules for StarRating
            RuleFor(x => x.StarRating).InclusiveBetween(1 , 5);

            //Rules for emails
            RuleFor(x => x.Email).NotEmpty();
            //RuleFor(x => x.Email).EmailAddress();
            When(x => !string.IsNullOrEmpty(x.Email), () => {
                RuleFor(x => x.Email).EmailAddress();
                 });

             //Rules for Username
             //Username should be between length 1 to 20 and cannot be empty and should only have alphabets (a-z and A-Z)
            RuleFor(x => x.UserName).NotEmpty();
            //  RuleFor(x => x.UserName).Matches("^[a-zA-Z]{1,20}$");
            When(x => !string.IsNullOrEmpty(x.UserName), () => {
                RuleFor(x => x.UserName).Matches("^[a-zA-Z]{1,20}$");
            });
        }

    }
}
