﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UltimateQuarry.Model
{
    public class UserFeedback : IEntityBase
    {
        public int Id { get; set; } 
        public string Feedback { get; set; } 
        public float StarRating { get; set; } 
        public string Email { get; set; }
        public string UserName { get; set; }
        public DateTime DateAndTime { get; set; }
        public DateTimeOffset DateTimeOffsetValue { get; set; }
    }
}
